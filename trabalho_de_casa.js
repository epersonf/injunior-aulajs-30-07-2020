//entrada
let alunos = [
    {
        "nome": "Nome1",
        "turma": "A",
        "nota1": 10,
        "nota2": 7
    },
    {
        "nome": "Nome4",
        "turma": "A",
        "nota1": 10,
        "nota2": 9
    },
    {
        "nome": "Nome2",
        "turma": "B",
        "nota1": 5,
        "nota2": 8
    },
    {
        "nome": "Nome5",
        "turma": "B",
        "nota1": 7,
        "nota2": 9
    },
    {
        "nome": "Nome3",
        "turma": "C",
        "nota1": 7,
        "nota2": 4
    }
];

//funcao que retorna nota do aluno passado como parametro
function CalculaNota(aluno) {
    let sum = 0;
    let amount = 0;
    for (let key in aluno) {
        if (!key.startsWith("nota")) continue;
        sum += aluno[key];
        amount++;
    }
    return sum/amount;
}

//index com dicionario com nome e index de cada turma
let turmas = ['A', 'B', 'C'];

//funcao para gerar lista de melhores alunos
function GetBest(listaDeAlunos) {
    let melhoresAlunos = [turmas.length];

    for (let i = 0; i < alunos.length; i++) {

        //calcula informacoes importantes
        let notaFinal = CalculaNota(alunos[i]);
        let turmaIndex = turmas.indexOf(alunos[i]["turma"]);
        
        //verifica se a turma existe
        if (turmaIndex == -1) continue;

        //verifica se nota final é maior que o maior da turma, caso contrario continua
        if (melhoresAlunos[turmaIndex] != undefined) 
            if (melhoresAlunos[turmaIndex]["notaFinal"] > notaFinal) continue;

        //adiciona o aluno na lista
        melhoresAlunos[turmaIndex] = {
            "nome": alunos[i]["nome"],
            "turma": alunos[i]["turma"],
            "notaFinal": notaFinal,
        };
    }
    return melhoresAlunos;
}

//mostra na tela resultado
let melhoresAlunos = GetBest(alunos);
for (let alunoIndex in melhoresAlunos) {
    if (melhoresAlunos[alunoIndex] == undefined) continue;
    let aluno = melhoresAlunos[alunoIndex];
    console.log("O melhor aluno da turma " + aluno["turma"] + " foi o aluno " + aluno["nome"] + " com média: " + aluno["notaFinal"] + ".");
}